#include "dllist.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

void generate(int n, DLList& m, int threadnum)
{
	int i, keyword;
	void* it;
	srand((unsigned)time(0));
	for (i = 0; i<n; i++)
	{
		keyword = (rand() % n + 1);
		it = &keyword;
		cout << "   input_Thread : " << threadnum << "\t";
		cout << i << "th item : "  << keyword << endl; 
		m.SortedInsert(it, keyword);
	}
}

void move(int n, DLList& m, int threadnum)
{
	int i = 0;
	
	 for(i = 0 ; i < n ; i++){ 
                 int *key = new int; 
                 void *item = m.Remove(key); 
                 if (item == NULL){ cout << "remove error" << endl; }
                 else{
		   cout << "   output_Thread : " << threadnum <<"\t";
                   cout << i << "th delete : " << *key << "\n"; 
             	 }
		 delete key; 
      } 

}

